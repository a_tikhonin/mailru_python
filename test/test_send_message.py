# -*- coding: utf-8 -*-


def test_send_message(app):
    app.auth_page.login("tikhonin_qalab", app.accounts['tikhonin_qalab'])
    app.send_page.open()
    app.send_page.send(to="tikhonin_qalab@mail.ru",
                       subject="Проверочное письмо",
                       body="Привет, почта")
