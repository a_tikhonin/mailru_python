class AuthPage:
    def __init__(self, app):
        self.app = app

    def login(self, username, password):
        self.app.wd.get("https://mail.ru/")
        self.app.wd.find_element_by_id("mailbox:login").click()
        self.app.wd.find_element_by_id("mailbox:login").clear()
        self.app.wd.find_element_by_id("mailbox:login").send_keys(username)
        self.app.wd.find_element_by_id("mailbox:password").click()
        self.app.wd.find_element_by_id("mailbox:password").clear()
        self.app.wd.find_element_by_id("mailbox:password").send_keys(password)
        self.app.wd.find_element_by_id("mailbox:password").send_keys(self.app.keys.RETURN)
