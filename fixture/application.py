# -*- coding: utf-8 -*-

from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from fixture.authorization import AuthPage
from fixture.mail import SendPage


class Application:

    def __init__(self):
        self.wd = WebDriver()
        self.keys = Keys()
        self.wd.implicitly_wait(60)

        self.accounts = dict()
        self.accounts['tikhonin_qalab'] = "Test7600"

        self.auth_page = AuthPage(self)
        self.send_page = SendPage(self)

    def destroy(self):
        self.wd.quit()
