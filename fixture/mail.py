class SendPage:
    def __init__(self, app):
        self.app = app

    def open(self):
        self.app.wd.find_element_by_link_text("Написать письмо").click()

    def send(self, to, subject, body):

        # Заполнение полей "Кому" и "Тема"
        self.app.wd.find_element_by_css_selector("textarea.js-input.compose__labels__input").click()
        self.app.wd.find_element_by_css_selector("textarea.js-input.compose__labels__input").send_keys(to)
        self.app.wd.find_element_by_name("Subject").click()
        self.app.wd.find_element_by_name("Subject").send_keys(subject)

        # Переход из поля "Тема" к телу письма через Tab и вставка текста
        self.app.wd.find_element_by_name("Subject").send_keys(self.app.keys.TAB + body)

        # CTRL + ENTER - Отправка письма
        self.app.wd.find_element_by_name("Subject").send_keys(self.app.keys.CONTROL, self.app.keys.RETURN)
